#ifndef ZER0_STACK_H
#define ZER0_STACK_H

#include <stddef.h>

typedef struct {
	size_t  cap;
	size_t  len;
	void   *data;
} zer0_stack;

void
zer0_stack_init(zer0_stack*, size_t);

void
zer0_stack_push(zer0_stack*, void*, size_t);

void*
zer0_stack_pop(zer0_stack*, size_t);

void
zer0_stack_free(zer0_stack*);

#endif
