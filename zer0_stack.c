#include "zer0_stack.h"

#include <malloc.h>
#include <string.h>

void
zer0_stack_init(zer0_stack *stack, size_t size) {
	stack->cap  = 4;
	stack->len  = 0;
	stack->data = malloc(stack->cap * size);
}

void
zer0_stack_push(zer0_stack *stack, void *value, size_t size) {
	if (stack->len == stack->cap) {
		stack->cap  *= 2;
		stack->data  = realloc(stack->data, stack->cap * size);
	}

	void *ptr;
	ptr = stack->data + stack->len * size;
	
	memcpy(ptr, value, size);	

	++stack->len;	
}

void*
zer0_stack_pop(zer0_stack *stack, size_t size) {
	if (stack->len == 0)
		return NULL;

	--stack->len;	
	
	void *ptr;
	ptr = stack->data + stack->len * size;

	return ptr;
}

void
zer0_stack_free(zer0_stack *stack) {
	free(stack->data);
}
