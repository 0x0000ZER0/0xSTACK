# 0xSTACK

A generic stack implementation using `void*`.

### API

- Creating a stack. For this process you need to specify the size of your element. Here is an example using `int`s.

```c
zer0_stack stack;
zer0_stack_init(&stack, sizeof (int));
//...
zer0_stack_free(&stack);
```

- For pushing an element you can use the `zer0_stack_push` function.

```c
int x0;
int x1;
int x2;
int x3;
int x4;
int x5;

x0 = 0x0;
x1 = 0x1;
x2 = 0x2;
x3 = 0x3;
x4 = 0x4;
x5 = 0x5;

zer0_stack_push(&stack, (void*)&x0, sizeof (int));
zer0_stack_push(&stack, (void*)&x1, sizeof (int));
zer0_stack_push(&stack, (void*)&x2, sizeof (int));
zer0_stack_push(&stack, (void*)&x3, sizeof (int));
zer0_stack_push(&stack, (void*)&x4, sizeof (int));
zer0_stack_push(&stack, (void*)&x5, sizeof (int));

printf("CAP: %zu\n", stack.cap);
printf("LEN: %zu\n", stack.len);
for (void *ptr = stack.data;
     ptr != (stack.data + stack.len * sizeof (int));
     ptr += sizeof (int)) {
        int val;
        val = *(int*)ptr;
        printf("[0x%01x]\n", val);
}
```

- For getting the last element use the `zer0_stack_pop` function. If the `length` is `0` then the function will return `NULL`.

```c
void *p0;
void *p1;

p0 = zer0_stack_pop(&stack, sizeof (int));
p1 = zer0_stack_pop(&stack, sizeof (int));

int v0;
int v1;

v0 = *(int*)p0;
v1 = *(int*)p1;

printf("[0x%01x]\n", v0);
printf("[0x%01x]\n", v1);
```
